import { AddPostUseCase } from '../usecases/add-post.usecase'
import { AllPostsUseCase } from '../usecases/all-posts.usecase'
import { DeletePostUseCase } from '../usecases/delete-post.usecase'
import { UpdatePostUseCase } from '../usecases/update-post.usecase'
import { BaseBloc } from './base'

export class PostsBloc extends BaseBloc {
  static instance = null

  static state = {
    posts: [],
    selectedPost: {}
  }

  constructor() {
    super('Posts_state')
    PostsBloc.instance = this
  }

  static getInstance() {
    if (!PostsBloc.instance) {
      PostsBloc.instance = new PostsBloc()
    }
    return PostsBloc.instance
  }

  async loadPosts() {
    const posts = await AllPostsUseCase.execute()
    this.setState({ posts: posts })
  }

  async createPost(post) {
    const posts = await AddPostUseCase.execute(this.getState().posts, post)
    this.setState({ posts })
  }

  async deletePost() {
    const selectedPost = this.getState().selectedPost
    const postsRemoved = await DeletePostUseCase.execute(
      this.getState().posts,
      selectedPost.id
    )
    this.setState({ posts: postsRemoved })
  }

  selectPost(post) {
    this.setState({ selectedPost: post })
  }

  async updatePost(post) {
    const selectedPost = this.getState().selectedPost
    const postWithUpdated = await UpdatePostUseCase.execute(this.getState().posts, {
      id: selectedPost.id,
      title: post.title,
      content: post.content
    })
    this.setState({ posts: postWithUpdated })
  }
}
