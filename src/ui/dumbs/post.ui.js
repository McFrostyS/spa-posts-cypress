import { LitElement, html } from 'lit'

export class PostUI extends LitElement {
  static get properties() {
    return {
      posts: { type: Array }
    }
  }

  selectPost(e, post) {
    this.dispatchEvent(
      new CustomEvent('selected-post', {
        detail: {
          selectedPost: post
        },
        bubbles: true
      })
    )
  }

  render() {
    return html`<ul id="posts">
      ${this.posts.map(
        (post) =>
          html`<li
            class="post"
            id="post_${post.postId}"
            @click="${(e) => this.selectPost(e, post)}"
          >
            ${post.title}
          </li>`
      )}
    </ul>`
  }

  createRenderRoot() {
    return this
  }
}

customElements.define('post-ui', PostUI)
