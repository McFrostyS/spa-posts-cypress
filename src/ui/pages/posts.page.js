import './../smarts/posts.component'
import { PostsBloc } from '../../core/blocs/posts.bloc'

export class PostsPage extends HTMLElement {
  async connectedCallback() {
    const postsBloc = PostsBloc.getInstance()
    await postsBloc.loadPosts()

    this.innerHTML = `
        <genk-posts></genk-posts>
    `
  }
}

customElements.define('posts-page', PostsPage)
