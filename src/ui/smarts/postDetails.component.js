import { LitElement, html } from 'lit'
import { PostsBloc } from '../../core/blocs/posts.bloc'

class PostDetails extends LitElement {
  async connectedCallback() {
    super.connectedCallback()
    const postBloc = PostsBloc.getInstance()
    this.handleState = (state) => {
      this.selectedPost = state.selectedPost
      this.requestUpdate()
    }
    postBloc.subscribe(this.handleState)
  }

  disconnectedCallback() {
    const postBloc = PostsBloc.getInstance()
    postBloc.unsubscribe(this.handleState)
  }

  render() {
    return html`
      <button @click="${this.cancel}">Add Post</button>
      ${this.selectedPost
        ? html`
            <div>
              <label for="post-title">Title:</label>
              <input id="post-title" .value="${this.selectedPost.title || ''}" />
              <br />
              <label for="post-content">Content:</label>
              <textarea
                id="post-content"
                .value="${this.selectedPost.content || ''}"
              ></textarea>
              <a href="#" @click="${this.cancel}">Cancel</a>
              <a href="#" @click="${(e) => this.updatePost(e, this.selectedPost)}"
                >Update</a
              >
              <a href="#" @click="${(e) => this.deletePost(e, this.selectedPost)}"
                >Delete</a
              >
            </div>
          `
        : html`
            <div>
              <label for="post-title">Title:</label>
              <input id="post-title" .value="${''}" />
              <br />
              <label for="post-content">Content:</label>
              <textarea id="post-content" .value="${''}"></textarea>
              <a href="#" @click="${this.cancel}">Cancel</a>
              <a href="#" class="addBtn" @click="${this.addPost}">Add</a>
            </div>
          `}
    `
  }

  cancel(e) {
    e.preventDefault()
    this.selectedPost = null
    this.requestUpdate()
  }

  async updatePost(event, selectedPost) {
    event.preventDefault()
    const title = this.querySelector('#post-title').value
    const content = this.querySelector('#post-content').value
    const postBloc = PostsBloc.getInstance()
    await postBloc.updatePost({
      id: selectedPost?.id,
      title: title,
      content: content
    })
    this.posts = postBloc.getState().posts
    this.notifyChangePosts(this.posts)
    this.cancel(event)
  }

  async deletePost(event, selectedPost) {
    event.preventDefault()
    const postBloc = PostsBloc.getInstance()
    postBloc.selectPost(selectedPost)
    await postBloc.deletePost()
    this.posts = postBloc.getState().posts
    this.notifyChangePosts(this.posts)
  }

  async addPost(event) {
    event.preventDefault()
    const title = this.querySelector('#post-title').value
    const content = this.querySelector('#post-content').value
    const postBloc = PostsBloc.getInstance()
    await postBloc.createPost({
      title: title,
      content: content
    })
    this.posts = postBloc.getState().posts
    this.notifyChangePosts(this.posts)
  }

  notifyChangePosts(posts) {
    this.dispatchEvent(
      new CustomEvent('change-posts', {
        detail: {
          posts: posts
        }
      })
    )
    this.clearForm()
  }

  clearForm() {
    this.querySelector('#post-title').value = ''
    this.querySelector('#post-content').value = ''
    const postBloc = PostsBloc.getInstance()
    postBloc.selectPost(null)
  }

  createRenderRoot() {
    return this
  }
}

customElements.define('outlet-post-detail', PostDetails)
