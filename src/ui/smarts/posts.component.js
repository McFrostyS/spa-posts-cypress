import { LitElement, html } from 'lit'
import { PostsBloc } from '../../core/blocs/posts.bloc'
import '../dumbs/post.ui'

export class PostsComponent extends LitElement {
  connectedCallback() {
    super.connectedCallback()
    const postBloc = PostsBloc.getInstance()
    postBloc.subscribe((state) => {
      this.posts = state.posts
      this.requestUpdate()
    })
  }

  disconnectedCallback() {
    super.disconnectedCallback()
    const postBloc = PostsBloc.getInstance()
    postBloc.unsubscribe(this.handleState)
  }

  handleSelectedPost(e) {
    const postBloc = PostsBloc.getInstance()
    postBloc.selectPost(e.detail.selectedPost)
  }

  render() {
    return html`
      <h1>Posts List</h1>
      <ul>
        <post-ui
          @selected-post="${(e) => this.handleSelectedPost(e)}"
          .posts="${this.posts}"
        ></post-ui>
      </ul>
    `
  }

  createRenderRoot() {
    return this
  }
}

customElements.define('genk-posts', PostsComponent)
