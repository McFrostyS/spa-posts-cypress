/// <reference types="cypress" />

describe('Posts Management', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080')
    cy.get('[href="/posts"]')
  })

  it('should add a post', () => {
    cy.get('button').click()
    cy.get('#post-title').type('New Post')
    cy.get('#post-content').type('New Content')
    cy.get('.addBtn').click()
    cy.get('div > :nth-child(8)').should('have.text', 'New Post')
  })

  it('should delete a post', () => {
    cy.get('#posts > :nth-child(1)').click()
    cy.get('div > :nth-child(8)').click()
    cy.get('div > :nth-child(8)').should('not.exist')
  })

  it('should update a post', () => {
    cy.get('#posts > :nth-child(1)').click()
    cy.get('#post-title').type('Updated Post')
    cy.get('div > :nth-child(7)').click()
    cy.get('div > :nth-child(8)').should('have.text', 'Updated Post')
  })
})
