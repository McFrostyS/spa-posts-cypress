import { PostsBloc } from '../../../src/core/blocs/posts.bloc'
import { Post } from '../../../src/core/model/post'
import { PostsRepository } from '../../../src/core/repositories/posts.repository'
import { POSTS } from '../../fixtures/posts'

jest.mock('../../../src/core/repositories/posts.repository')

describe('Posts Bloc', () => {
  beforeEach(() => {
    PostsRepository.mockClear()
  })

  it('should load posts', async () => {
    PostsRepository.mockImplementation(() => {
      return {
        getAllPosts: () => {
          return POSTS
        }
      }
    })

    const bloc = PostsBloc.getInstance()
    await bloc.loadPosts()
    const posts = bloc.getState().posts

    expect(posts).toHaveLength(POSTS.length)
  })

  it('should create post', async () => {
    const post = new Post({
      postId: 101,
      title: 'Im title 101',
      content: 'Im body 101'
    })

    PostsRepository.mockImplementation(() => {
      return {
        createPost: jest.fn().mockReturnValue({
          postId: post.postId,
          title: post.title,
          body: post.content
        }),
        getAllPosts: jest.fn().mockReturnValue(POSTS)
      }
    })

    const bloc = PostsBloc.getInstance()
    await bloc.loadPosts()
    const posts = bloc.getState().posts

    await bloc.createPost(post)

    const postsUpdated = bloc.getState().posts

    expect(postsUpdated).toHaveLength(posts.length + 1)
    expect(postsUpdated[0].title).toBe(post.title)
    expect(postsUpdated[0].content).toBe(post.content)
  })

  it('should delete post', async () => {
    const post = new Post({
      postId: 101,
      title: 'Im title 101',
      content: 'Im body 101'
    })

    PostsRepository.mockImplementation(() => {
      return {
        createPost: jest.fn().mockResolvedValue({
          postId: 101,
          title: post.title,
          body: post.content
        }),
        getAllPosts: jest.fn().mockReturnValue(POSTS),
        deletePost: jest.fn()
      }
    })

    const bloc = PostsBloc.getInstance()
    await bloc.loadPosts()
    const posts = bloc.getState().posts

    await bloc.createPost(post)

    bloc.selectPost(post)

    await bloc.deletePost()
    const postsDeleted = bloc.getState().posts

    expect(postsDeleted.length).toBe(posts.length)
  })

  it('should update post', async () => {
    const post = new Post({
      postId: 101,
      title: 'Im title 101',
      content: 'Im body 101'
    })

    PostsRepository.mockImplementation(() => {
      return {
        createPost: jest.fn().mockResolvedValue({
          postId: 101,
          title: post.title,
          body: post.content
        }),
        getAllPosts: jest.fn().mockReturnValue(POSTS),
        updatePost: jest.fn().mockResolvedValue({
          postId: 101,
          title: 'Im title 101 updated',
          body: 'Im body 101 updated'
        })
      }
    })
    const postWithChanges = {
      postId: 101,
      title: 'Im title 101 updated',
      content: 'Im body 101 updated'
    }

    const bloc = PostsBloc.getInstance()
    await bloc.loadPosts()
    const posts = bloc.getState().posts

    await bloc.createPost(post)
    const postsWithNewPost = bloc.getState().posts

    bloc.selectPost(post)

    await bloc.updatePost(postWithChanges)
    const postsUpdated = bloc.getState().posts

    expect(postsUpdated.length).toBe(postsWithNewPost.length)
  })
})
